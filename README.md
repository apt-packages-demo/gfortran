# gfortran

GNU Fortran Compiler https://gcc.gnu.org/fortran/

## Similar packages
### Debian
#### Flang
* Since Debian 10 (2019) buster https://packages.debian.org/en/buster/flang-7
  * Check if virtual package flang exists...
* https://github.com/flang-compiler/flang
* [*FLANG: NVIDIA Brings Fortran To LLVM*](https://www.phoronix.com/scan.php?page=news_item&px=LLVM-NVIDIA-Fortran-Flang)
* [*Building the LLVM Flang Fortran compiler*](https://www.scivision.co/flang-compiler-build-tips/)
#### f18
* https://www.google.com/search?q=f18+fortran+site:debian.org
### Other distribution
* https://gitlab.com/alpinelinux-packages-demo/gfortran
### Source packages
* https://www.google.com/search?q=Fortran+compiler+front-end+for+LLVM
#### f18
* https://github.com/flang-compiler/f18
* [*NVIDIA Has Been Working On A New Fortran "f18" Compiler It Wants To Contribute To LLVM*](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-F18-Fortran-Compiler)